class Carta
  attr_reader :numero, :naipe

  # =============================================
  #  construtor: int numero -> String naipe -> 0
  #  
  #  Cria uma nova carta com um determinado
  #  número e naipe.
  # =============================================
  def initialize(numero, naipe)
    @numero = numero
    @naipe = naipe
  end

  # =============================================
  #  traduzir_numero: int numero -> int || String
  #  
  #  Traduz o número para o símbolo da figura,
  #  estas podendo ser ás, valete, dama ou rei.
  #
  #  Caso o número não represente nenhum desses
  #  símbolos, o próprio número será retornado.
  # =============================================
  def traduzir_numero(numero)
    return 'A' if numero == 1  # Ás
    return 'B' if numero == 11 # Valete
    return 'Q' if numero == 12 # Dama
    return 'K' if numero == 13 # Rei

    return numero
  end

  # =============================================
  #  carta: String
  #  
  #  Retorna o número e o naipe da carta criada.
  # =============================================
  def carta
    return "#{traduzir_numero(@numero)}#{@naipe}"
  end
end